Демонстрация второй лабораторной работы с использованием юнит-тестирования _boost_.

Файлы геометрических фигур находятся в каталоге [_common_](https://gitlab.com/alexeit/cpp_practice/tree/master/common).

Установка _boost_ описана в [инструкции из данного проекта](https://gitlab.com/alexeit/cpp_practice/tree/master/user_manual_gitlab_environment). Ниже приводится выдержка:

# Boost
**Boost** - это большая библиотека для решения различных задач на C++. В курсе лабораторных она будет применяться для автоматизированного тестирования. Особенность _boost_ является то, что очень большая ее часть содержится в заголовочных файлах (header).

Можно отложить установку _boost_ до времени, когда ты выполнишь первые лабораторные и освоишься в _git_ окружении.

## **Windows**
Установка boost в Windows - не совсем тривиальная задача. Действуй по ссылке <https://www.boost.org/doc/libs/1_55_0/more/getting_started/windows.html#the-boost-distribution> или попробуй найти руководство на родном языке. Эта установка будет работать для разработки под Visual Studio. 

Для установки _boost_ в _Git Bash_ обратись к информации, подобной этой: <https://stackoverflow.com/questions/20265879/how-do-i-build-boost-1-55-with-mingw>

Для установки _boost_ в _MSYS2_ смотри эту ссылку: https://github.com/orlp/dev-on-windows/wiki/Installing-GCC--&-MSYS2

## **Linux**
В Linux _boost_ устанавливается пакетным менеджером. Например, для Debian (Ubuntu, Mint) выполни команду:

> sudo apt install boost

## **MacOS**
В MacOS _boost_ устанавливается как пакет, портированный из Linux. Это делается с помощью **brew**. Официальное руководство от _Apple_ по этой ссылке: <http://macappstore.org/boost/>


# Polygon references

## общая библиотека: 
<https://github.com/mapbox/earcut.hpp>

## общее описание codeforces: 
<https://codeforces.com/blog/entry/48868>

## centroid:
<https://stackoverflow.com/questions/2792443/finding-the-centroid-of-a-polygon> (???	 

## convex (выпуклость):
<http://csharphelper.com/blog/2014/07/determine-whether-a-polygon-is-convex-in-c/>
<https://stackoverflow.com/questions/471962/how-do-i-efficiently-determine-if-a-polygon-is-convex-non-convex-or-complex>

## area: 
<https://stackoverflow.com/questions/16285134/calculating-polygon-area>
<https://stackoverflow.com/questions/24467972/calculate-area-of-polygon-given-x-y-coordinates>
<https://stackoverflow.com/questions/451426/how-do-i-calculate-the-area-of-a-2d-polygon>

## scale:
<https://stackoverflow.com/questions/6830480/scaling-an-arbitrary-polygon>
<https://en.wikipedia.org/wiki/Scaling_%28geometry%29>
<https://en.wikipedia.org/wiki/Homothetic_transformation>
<https://stackoverflow.com/questions/31125511/scale-polygons-by-a-ratio-using-only-a-list-of-their-vertices>
<https://math.stackexchange.com/questions/1889423/calculating-the-scale-factor-to-resize-a-polygon-to-a-specific-size>

## Пример реализации в коде, часть алгоритмов вычислений заимствована со Stack Overflow
Смотри здесь _main.cpp_ и _../common/polygon.hpp_, _../common/polygon.cpp_