#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"


void showRectangle(rectangle_t rect)
{
	std::cout << "width: " << rect.width << ", height: " << rect.height;
	std::cout << ", x: " << rect.pos.x << ", y: " << rect.pos.y;
	std::cout << std::endl;
}

void show(const Shape & shape)
{
	std::cout << __LINE__ << ": " << shape.getArea() << std::endl;
	showRectangle(shape.getFrameRect());
}

int main()
{
	Rectangle rect (10.0, 5.0, {.x = 15.0, .y = 15.0});
	show(rect);
	rect.move({.x = -10, .y =-5});
	show(rect);

	Circle circle (5.0, {.x = 1.0, .y = 2.0 });
	show(circle);


	Shape * shapes[2] = { &rect, &circle };
	//Shape * shape = nullptr;

	int sizeShapes = sizeof(shapes)/sizeof(size_t);
	std::cout << "sizeShapes: " << sizeShapes << std::endl;

	for ( int i = 0; i < sizeShapes; ++i ) {
	    //show(*shapes[i]);
	    std::cout << __LINE__ << ": " << shapes[i]->getArea() << std::endl;
	}	

	// Демонстрация полиморфизма из лекции Алексея Валерьевича по ООП 2019-04-01 
	// слайд 36

	Rectangle r = Rectangle(10, 10, {0, 0});

	r.getArea();

	Shape & s = r;

	s.getArea();



	/*
	О виртуальном деструкторе: такое использование определенно 
	требует виртуального деструктора, исходя, например, из:
	https://isocpp.org/wiki/faq/virtual-functions#virtual-dtors
	Однако, проверить это на практике в коде мне не удается:
	поведение всегда корректно - 
	здесь 'delete ptrRect' вызывает деструктор ~Shape
	и утечек памяти нет.
	*/
	Rectangle *ptrRect = new Rectangle(10.0, 5.0, { 15.0, 15.0 });
	delete ptrRect;

	return 0;
	
}
	
