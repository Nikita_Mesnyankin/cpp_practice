#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace tchervinsky {

  class Rectangle : public Shape
  {
  public:
    Rectangle(double width, double height, const point_t & pos); // для pos используем ссылку, так как это - структура
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override; // смещение в заданную точку
    void move(double x, double y) override; // смещение по осям
    void scale(const double coefficient) override; // масштабирование
  private:
    rectangle_t rect_;
  };

} // namespace tchervinsky
#endif // RECTANGLE_HPP
