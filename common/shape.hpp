#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace tchervinsky {

  class Shape
  {
  public:
    virtual ~Shape() = default; // деструктор по умолчанию
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & pos) = 0; // смещение в заданную точку
    virtual void move(double x, double y) = 0; // смещение по осям
    virtual void scale(const double coefficient) = 0;  // масштабирование
  };

} // namespace tchervinsky
#endif // SHAPE_HPP
