#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  try {
    drozdov::CompositeShape composite1;
    std::cout << "\nСreated an empty first composition.";
    composite1.printData();

    drozdov::Rectangle rectangle1(7.2, 5.7, {5, -2.4});
    drozdov::Rectangle rectangle2(3.0, 5.0, {-1.0, -3.0});
    drozdov::Circle circle1(5, {6, 7.2});

    composite1 += rectangle1;
    composite1 = composite1 + rectangle2;
    composite1.addShape(circle1);
    std::cout << "\nAfter (+= rectangle1) and (+ rectangle2) and (addShape(circle1)) in first composite:";
    composite1.printData();
    std::cout << "\nArea: " << composite1.getArea();
    std::cout << "\n*****************************************************";

    drozdov::CompositeShape composite2(std::move(composite1));
    std::cout << "\nCreated a new composition that took the values from the first composition.";
    std::cout << "\nAbout first composite:";
    composite1.printData();
    std::cout << "\nNumber of figures in the second composition: " << composite2.getQuantity();

    std::cout << "\nСopy assignment demonstration";
    composite1 = composite2;
    std::cout << "\nNumber of figures in the first composition: " << composite1.getQuantity();
    std::cout << "\nNumber of figures in the second composition " << composite2.getQuantity();

    composite2.removeShape(2);
    const double dx = 4.3;
    const double dy = 10.0;
    composite2.move(dx,dy);
    std::cout << "\nAfter removing the second rectangle and moving the composition +(" << dx << ';' << dy << ')';
    std::cout << "\nNumber of figures in second composition: " << composite2.getQuantity();

    std::cout << "\nThe frame-rectangle of the second composition";
    const drozdov::rectangle_t frameRectComp2 = composite2.getFrameRect();
    std::cout << "\nWidth: " << frameRectComp2.width;
    std::cout << "\nHeight: " << frameRectComp2.height;
    std::cout << "\nPosition: (" << frameRectComp2.pos.x << ';' << frameRectComp2.pos.y << ')';
    std::cout << "\n*****************************************************";

    drozdov::CompositeShape composite3(composite2);
    std::cout << "\nCreated a new composite that copy the values from the second composition.";
    const drozdov::point_t posComp3 = composite3.getPos();
    if (frameRectComp2.pos.x == posComp3.x && frameRectComp2.pos.y == posComp3.y) {
      std::cout << "\nThe position of the third composition with center of the frame second composition converge.";
    } else {
      std::cout << "\nThe position of the third composition with center of the frame second composition no converge.";
    }

    const double coefficientScale = 2.0;
    composite3.scale(coefficientScale);
    std::cout << "\nАfter scaling the third composition in " << coefficientScale;
    composite3.printData();
    composite3.move({dx,dy});
    std::cout << "\nAfter moving the composition on new position: (" << dx << ';' << dy << ')';
    composite3.printData();

    std::cout << "\n*****************************************************";
    drozdov::Circle startCircle(3.0, {1.0, 2.0});
    drozdov::CompositeShape composite4(startCircle);
    std::cout << "\nСreated a new composition with the original definition of the first figure.";
    composite4.printData();

    std::cout << "\nDemonstration of assignment with movement.";
    composite4 = std::move(composite3);
    std::cout << "\nThat's what's inside the fourth composite.";
    composite4.printData();

    std::cout << std::endl;
    return 0;
  } catch (const std::invalid_argument &ex) {
    std::cerr << "\nError invalid-argument: \"" << ex.what() << "\"";
    return 1;
  } catch (const std::out_of_range &ex) {
    std::cerr << "\nError in index out of range: \"" << ex.what() << "\"";
    return 1;
  } catch (const std::logic_error &ex) {
    std::cerr << "\nError in logic of programm: \"" << ex.what() << "\"";
    return 1;
  } catch (const std::bad_alloc &ex) {
    std::cerr << "\nMemory is full: \"" << ex.what() << "\"";
    return 2;
  } catch (const std::exception &ex) {
    std::cerr << "\nSystem error:  \"" << ex.what() << "\"";
    return 2;
  }
}
